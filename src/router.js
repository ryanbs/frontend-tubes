import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function importComponent(path) {
   return () => import(`./components/${path}.vue`)
}

Vue.use(VueRouter);

const router = new VueRouter({
   mode: "history",
   routes: 
   [{
    path: "/",
    name: "Dashboard",
    meta: {title: 'Dashboard'},
    component: importComponent('Dashboard'),
    },
    {
        path: "/nav",
        name: "Navbar",
        component: importComponent('Navbar'),
        children: [
            {
                path: "/home",
                name: "Home",
                meta: {title: 'Home'},
                component: importComponent('Home'),
            },
            {
                path: "/profile",
                name: "Profile",
                meta: {title: 'Profile'},
                component: importComponent('Profile'),
            },
            {
                path: "/items",
                name: "Items",
                meta: {title: 'Items'},
                component: importComponent('Items'),
            },
            {
                path: "/wishlist",
                name: "wishlist",
                meta: {title: 'Wishlist'},
                component: importComponent('Wishlist'),
            },
            {
                path: "/Cart",
                name: "cart",
                meta: {title: 'Cart'},
                component: importComponent('Cart'),
            },
        ]
    },
    {
    path: "/admin",
    name: "Admin",
    meta: {title: 'Admin'},
    component: importComponent('Admin'),
    },
    {
        path: '*',
        redirect:'/'
    },
]
});

router.beforeEach((to, from, next)=> {
    document.title = to.meta.title
    next()
});
export default router;
